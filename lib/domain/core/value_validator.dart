import 'package:dartz/dartz.dart';
import 'package:tododdrflutter/domain/core/failures.dart';

Either<ValueFailure<String>, String> validateEmailAddress(String email) {
  const emailRegex = '\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b';

  if (RegExp(emailRegex).hasMatch(email)) {
    return right(email);
  } else {
    return left(ValueFailure.invalidEmail(failedValue: email));
  }
}

Either<ValueFailure<String>, String> validatePassword(String password) {
  if (password.length > 6) {
    return right(password);
  } else {
    return left(ValueFailure.shortPassword(failedValue: password));
  }
}
